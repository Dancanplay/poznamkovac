function countsetup() {
textarea = document.getElementById('poznamka');
document.getElementById('pocetznaku').innerHTML = textarea.value.length + "/250";
textarea.addEventListener('keyup', update, false);
textarea.addEventListener('keydown', update, false);
}

function update() {
  document.getElementById('pocetznaku').innerHTML = this.value.length + "/250";
  if(this.value.length > 250) {
    document.getElementById("pocetznaku").style.color = "red";
    document.getElementById("sendbutton").disabled = true;
  } else {
    document.getElementById("pocetznaku").style.color = "white";
    document.getElementById("sendbutton").disabled = false;
  }
};

function deletepoznamka(poznamkaid) {
    if(window.confirm("Opravdu chcete smazat tuto poznámku?")) window.location.href = window.location.origin + "/smazat/" + poznamkaid;
}