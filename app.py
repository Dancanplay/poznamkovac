import sqlite3
import os
from flask import Flask, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import TextAreaField, SelectField
from wtforms.validators import DataRequired

app = Flask(__name__)
app.debug = True
app.secret_key = "pepepege repega"

databaze = (os.path.join(os.path.abspath(os.path.dirname(__file__)), 'poznamky.db'))

class PoznamkaForm(FlaskForm):
    vyber = [('0', 'Méně důležité'), ('1', 'Normální'), ('2', 'Více důležité')]
    poznamka = TextAreaField("Poznámka", validators=[DataRequired()])
    dulezitost = SelectField(u'Důležitost', choices=vyber, default=1)

@app.route('/poznamka/vlozit/', methods=['GET', 'POST'])
def pridat_poznamku():
    """Zobrazí formulář pro vytvoření poznámky."""
    action = "/poznamka/vlozit/"
    form = PoznamkaForm()
    poznamka = form.poznamka.data
    dulezitost = form.dulezitost.data
    if form.validate_on_submit():
        conn = sqlite3.connect(databaze)
        c = conn.cursor()
        c.execute("INSERT INTO poznamky(text, priority, date) VALUES (?, ?, datetime('now','localtime'))", (poznamka, dulezitost))
        conn.commit()
        conn.close()
        return redirect('/')
    return render_template('add.html', form=form, title="Přidání poznámky", action=action)

@app.route('/poznamka/editovat/<int:poznamka_id>', methods=['GET', 'POST'])
def upravit_poznamku(poznamka_id):
    """Upraví poznámku"""
    action="/poznamka/editovat/" + str(poznamka_id)
    conn = sqlite3.connect(databaze)
    c = conn.cursor()
    c.execute("SELECT text,priority FROM poznamky WHERE rowid=?", (poznamka_id,))
    prefill = c.fetchone()
    conn.close()

    form = PoznamkaForm()
    poznamka = form.poznamka.data
    dulezitost = form.dulezitost.data
    if form.validate_on_submit():
        conn = sqlite3.connect(databaze)
        c = conn.cursor()
        c.execute("UPDATE poznamky SET text=?, priority=?, date=datetime('now','localtime') WHERE rowid=?", (poznamka, dulezitost, poznamka_id))
        conn.commit()
        conn.close()
        return redirect('/')
    return render_template('add.html', form=form, title="Upravit poznámku", action=action, prefill=prefill)

@app.route('/')
def zobraz_poznamky():
    """Zobrazí poznámky."""
    conn = sqlite3.connect(databaze)
    c = conn.cursor()
    c.execute("SELECT rowid, text, strftime('%d.%m.%Y %H:%M', date), priority FROM poznamky ORDER BY date DESC")
    poznamky = c.fetchall()
    conn.close()
    return render_template('poznamky.html', poznamky=poznamky)


@app.route('/smazat/<int:poznamka_id>')
def smaz_poznamku(poznamka_id):
    """Smaže vybranou poznámku"""
    conn = sqlite3.connect(databaze)
    c = conn.cursor()
    c.execute("DELETE FROM poznamky WHERE rowid=?", [poznamka_id])
    conn.commit()
    conn.close()
    return redirect('/')


if __name__ == '__main__':
    app.run()
